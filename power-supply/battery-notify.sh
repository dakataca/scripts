#!/bin/bash
# 🌟🌟🌟 🔋 Script Bash que administra las notificaciones del estado de la batería de un portátil 🌟🌟🌟 
#   https://gitlab.com/dakataca/scripts/-/tree/master/power-supply?ref_type=heads
# 📂 /$HOME/.local/bin/battery-notify.sh
# 🛠️ /$HOME/.config/systemd/user/battery-notify.service
# ⏱️  /$HOME/.config/systemd/user/battery-notify.timer
# 🔧 /etc/udev/rules.d/99-battery-notify.rules
#   battery-notify.sh

## Script for Manage Power Supply notifications with notify-send in Xorg sessions.
# DEPENDENCES: sudo pacman -S bc acpi ttf-font-awesome ttf-nerd-fonts-symbols


# Obtener nombre del dispositivos de energía.
get_device_label(){
    local -r power_supply="$1" status_file="$2"
    local -r device_label=$(find "$power_supply"/*/ -name "$status_file" 2>/dev/null | sed -E 's;.+/power_supply/(.+)/'"$status_file"';\1;')
    echo "$device_label"
}

# Obtener ruta de los ficheros de estado de los dispositivos de energía (charger and battery).
get_status_file_path() {
    local -r power_supply="$1" status_file="$2"
    local -r device_label=$(get_device_label "$power_supply" $status_file)
    echo "$power_supply/$device_label/$status_file"
}

# Obtener estado actual del cargador del portátil (0 or 1).
get_current_charger_status() {
    local -r power_supply="$1" param_charger_status="$2" online_file='online'
    local -r charger_online_file=$(get_status_file_path "$power_supply" "$online_file")
    local charger_status="${param_charger_status:-$(cat "$charger_online_file")}"
    echo $charger_status
}

# Obtener nivel de carga actual de la batería.
get_current_battery_level() {
    local -r power_supply="$1" capacity_file='capacity'
    local -r battery_capacity_file=$(get_status_file_path "$power_supply" "$capacity_file")
    local -r battery_capacity="$(cat "$battery_capacity_file")"
    echo $battery_capacity 
}

# Obtener estado actual de la batería (Full, Not charging, Charging, Discharging).
get_current_battery_status() {
    local -r power_supply="$1" status_file='status'
    local -r battery_status_file=$(get_status_file_path "$power_supply" "$status_file")
    local -r battery_status=$(cat "$battery_status_file")
    echo "$battery_status"
}

# Tiempo de transición durante el cambio de estado de la batería según carga del sistema.
time_battery_status_change() {
    local -r nproc=$(nproc) load_average=$(cat /proc/loadavg | sed "s/ .*//")
    local -r load_percentage=$(bc <<<"scale=2; 1.5*100/$nproc")
    local -r time_change=$(bc <<<"scale=2; 0.01*$load_percentage+0.6")
    echo $time_change
}

# Obtener estado oficial de la batería.
get_battery_status(){
    local -r power_supply="$1" current_charger_status=$2 current_battery_status="$3"
    local new_battery_status='Not charging' time_change=$(time_battery_status_change) new_charger_status=-1 limit_seconds=0 count=0 num_limit=0

    if [ "$current_battery_status" != 'Not charging' ]; then
        new_battery_status="$current_battery_status"; new_charger_status=$current_charger_status;
        num_limit=$(bc <<<"($time_change + 0.7) / 1 + 2")
        until ([ "$new_battery_status" == 'Discharging' ] && (( new_charger_status == 0 ))) || \
                ([[ "$new_battery_status" =~ ^(Full|Charging)$ ]] && (( new_charger_status == 1 ))) || \
		(( count == num_limit )); do
            ((count++))
            new_charger_status=$(get_current_charger_status "$power_supply" $current_charger_status)
	    new_battery_status="$(get_current_battery_status "$power_supply")"
            sleep $time_change 
        done
    elif [ "$current_battery_status" == 'Not charging' ]; then
	limit_seconds=$(bc <<<"(($time_change + 2)^2 - 2) +0.3" | cut -d'.' -f1)
        for (( i=1; i<=$limit_seconds; i++ )); do
            sleep $time_change 
            new_battery_status="$(get_current_battery_status "$power_supply")"
	    [ "$new_battery_status" != "$current_battery_status" ] && break
	done
    fi
    echo "$new_battery_status"
}

# Obtener título para la notificación a partir del estado de la batería.
get_title_notification() {
    local -r battery_status="$1" battery_level="$2"
    local title_notification=""
    local -rA bat_level=(
        [critical]=10
        [low]=20
        [complete]=100
    )
    case "$battery_status" in
        'Charging') title_notification=' Cargando batería';;
        'Discharging')
            if (( battery_level == bat_level[complete] )); then
                title_notification=' Batería completa'
            elif (( battery_level > bat_level[low] )); then
                title_notification=' Descargando batería'
            elif (( battery_level >= bat_level[critical] )); then
                title_notification=' Batería baja'
            elif (( battery_level < bat_level[critical] )); then
                title_notification=' Batería en nivel crítico'
            fi
            ;;
        'Full') title_notification=' Batería cargada';;
        'Not charging') title_notification=' Batería no recibe carga';;
    esac
    echo "$title_notification  (${battery_level}%)"
}

# Redondear nivel de batería a su décima más próxima.
round_battery_level() {
    local -r battery_level=$1 rounding_number=2 
    local rounded_battery_level=-1

    if [ -n "$battery_level" ]; then
        if (( battery_level >= 0 )) && (( battery_level <= 100 )); then
	    rounded_battery_level=$((($battery_level + $rounding_number) / 10 * 10));
        elif (( battery_level > 100 )); then
	    rounded_battery_level=100;
        fi
    fi
    echo $rounded_battery_level
}

# Obtener label apropiado para el icono de batería correspondiente, de acuerdo a los estados de la batería distintos de 'Not charging'.
get_label_charging() {
    local -r charger_status=$1 battery_status="$2" rounded_battery_level=${3:-0}
    local label_charging='-charging'

    if [ "$battery_status" == 'Charging' ] && (( rounded_battery_level <= 90 )); then
	    label_charging='charging'
    elif [ "$battery_status" == 'Discharging' ] || ([ "$battery_status" == 'Full' ] && (( charger_status == 0 ))); then
	    label_charging=""
    elif ([ "$battery_status" == 'Full' ] && (( charger_status == 1 ))) || \
	    ([ "$battery_status" == 'Charging' ] && (( rounded_battery_level >= 100 ))); then
		label_charging='charged'
    elif [ "$battery_status" == 'Not charging' ]; then
        label_charging='symbolic'
    fi
    echo "$label_charging"
}

# Obtener icono correspondiente al estado y nivel de la batería.
get_battery_level_icon() {
    local -r charger_status=$1 battery_status="$2" battery_level=$3
    local rounded_battery_level label_charging='' battery_icon='battery-level'

    case "$battery_status" in
        'Charging'|'Full')
            rounded_battery_level=$(round_battery_level $battery_level)
            label_charging=$(get_label_charging $charger_status "$battery_status" $rounded_battery_level)
            battery_icon+="-${rounded_battery_level}-${label_charging}-symbolic";;
        'Discharging')
            rounded_battery_level=$(round_battery_level $battery_level)
            battery_icon+="-${rounded_battery_level}-symbolic";;
        'Not charging'|*)
            label_charging=$(get_label_charging $charger_status "$battery_status")
            if (( battery_level >= 90 )); then
	        battery_icon="${battery_icon%-*}-full-charged-$label_charging"
            else
	        battery_icon="${battery_icon%-*}-caution-$label_charging"
	    fi
    esac
    echo "$battery_icon"
}

# Obtener el tiempo seguido de su string, EJ: "5 horas" ó "4 minutos" ó "14 segundos".
get_str_time() {
    local -r time=$1 time_type="$2"
    local str_time=''

    if (( time != 0 )); then
        if (( time == 1 )); then
            shopt -s extglob 
	    str_time="$time ${time_type::-1}"
            shopt -u extglob 
        elif (( time > 1 )); then
	    str_time="$time $time_type"
        else
	    str_time="Tiempo \"$time\" errado!"
	fi
    fi
    echo "$str_time"
}

# Obtener información de acpi.
get_acpi_output(){
    local -r num_limit=4
    local acpi_output=$(acpi) count=0
    while [[ "$acpi_output" =~ "will never fully discharge" ]] && (( count <= num_limit )); do
	sleep 1
	((count++))
        acpi_output=$(acpi)
    done
    echo "$acpi_output"
}

# Obtener tiempo restante.
get_time_remaining() {
    local -r acpi_output="$(get_acpi_output)"
    local time_remaining=''

    if [[ ! "$acpi_output" =~ "will never fully discharge" ]]; then 
        local -r time=($(sed -E "s/.* ([0-5][0-9]):([0-5][0-9]):([0-5][0-9]).*/\1 \2 \3/" <<<"$acpi_output"))
        shopt -s extglob ; local -r hours=${time[0]##0} minutes=${time[1]##0} seconds=${time[2]##0} ; shopt -u extglob
        local -r str_hours="$(get_str_time $hours 'horas')"  str_minutes="$(get_str_time $minutes 'minutos')" str_seconds="$(get_str_time $seconds 'segundos')" 

        if (( hours != 0 )); then
            if (( minutes != 0 )) && (( seconds != 0 )); then
                time_remaining="${str_hours}, ${str_minutes}, ${str_seconds}"
            elif (( minutes != 0 )); then
                time_remaining="${str_hours}, ${str_minutes}"
            elif (( seconds != 0 )); then
                time_remaining="${str_hours}, ${str_seconds}"
            fi
        else
            if (( minutes != 0 )) && (( seconds != 0 )); then
                time_remaining="${str_minutes}, ${str_seconds}"
            elif (( minutes != 0 )); then
                time_remaining="${str_minutes}"
            elif (( seconds != 0 )); then
                time_remaining="${str_seconds}"
            fi
        fi
    else
        time_remaining='Cargando a velocidad cero, nunca se cargará por completo.'
    fi
    echo "$time_remaining"
}

# Obtener cuerpo de la notificación.
get_head_notification() {
    local -r current_battery_status="$1" battery_status="$2" battery_level=$3
    local battery_time_remaining='' head_notification='' str_remaining='restantes' ac_mode='Funcionando en modo AC.' acpi="$(acpi)"

    case "$battery_status" in
        'Charging')
            # Si el cargador de corriente viene de estar desconectado a estar conectado mandar como head_notification "Calculando tiempo de carga..."
	    # y cuando el tiempo de carga se estabilice, volver a notificar con el tiempo calculado y estabilizado.
	    [ "$current_battery_status" != 'Charging' ] && sleep 4
            battery_time_remaining=$(get_time_remaining)
            head_notification="$battery_time_remaining para carga completa.";;
        'Discharging')
            battery_time_remaining=$(get_time_remaining)
	    [ "${battery_time_remaining: -1}" != 's' ] && str_remaining='restante'
            head_notification="$battery_time_remaining $str_remaining.";;
        'Full')
            head_notification="$ac_mode";;
        'Not charging'|*)
            if (( battery_level >= 90 )); then
                head_notification="$ac_mode"
            else
                head_notification='Posible avería.'
	    fi
    esac
    echo "$head_notification"
}

# Notificar estado del cargador y batería usando notify-send.
notify_status() {
    local -r mode=$1 battery_level_icon="$2" title_notification="$3" head_notification="$4"
    notify-send -r 847 -u "$mode" -i "$battery_level_icon" "$title_notification" "$head_notification"   # BAT es parecido a 847.
}

# Obtener modo de la notificación.
get_mode_notification() {
    local -r battery_status="$1" battery_level=$2
    local mode='normal' 

    if [ "$battery_status" == 'discharging' ] && (( battery_level <= critical_battery_level )); then
        mode='critical'
    fi
    echo "$mode"
}

# Validar fuente de ejecución.
validate_exec_source() {
    local -r mode_notification="$1" battery_level_icon="$2" title_notification="$3" head_notification="$4" exec_source="$5" \
	    critical_battery_level=$CRITICAL_BATTERY_LEVEL
    local battery_full_notify_file='' directory='' file=''

    if [ "$exec_source" == 'udev' ] || [ "$exec_source" == 'manual' ]; then
        notify_status $mode_notification "$battery_level_icon" "$title_notification" "$head_notification"
    elif [ "$exec_source" == 'systemd' ]; then
	if (( battery_level <= critical_battery_level )) && [ "$battery_status" == 'Discharging' ]; then
            notify_status $mode_notification "$battery_level_icon" "$title_notification" "$head_notification"
	elif (( battery_level >= 100 )) && [ "$battery_status" == 'Charging' ]; then
            directory='/tmp/power_supply'
	    file="$directory/battery_full_notify"
	    if [ ! -f "$file" ]; then
                notify_status $mode_notification "$battery_level_icon" "$title_notification" "$head_notification" && \
			mkdir -p "$directory" && touch "$directory/$file" 
            else
                echo -e "$title_notification - $head_notification"
	    fi
        else
            echo -e "$title_notification - $head_notification"
	fi
    fi
}

main() {
    local -r power_supply="$1" charger_status=$2 exec_source="$3"
    local -r current_charger_status="$(get_current_charger_status "$power_supply" $charger_status)" \
	    battery_level=$(get_current_battery_level "$power_supply") current_battery_status="$(get_current_battery_status "$power_supply")"
    local -r battery_status="$(get_battery_status "$power_supply" $current_charger_status "$current_battery_status")"
    local -r mode_notification=$(get_mode_notification "$battery_status" "$battery_level") \
	    battery_level_icon="$(get_battery_level_icon $current_charger_status "$battery_status" $battery_level)" \
            title_notification="$(get_title_notification "$battery_status" "$battery_level")" \
	    head_notification="$(get_head_notification "$current_battery_status" "$battery_status" "$battery_level")"

    validate_exec_source "$mode_notification" "$battery_level_icon" "$title_notification" "$head_notification" "$exec_source"
}

# Validar existencia de batería.
validate_battery(){
    local -r power_supply="$1" charger_status=$2 exec_source="$3" status_file='capacity'
    local -r battery=$(get_device_label "$power_supply" "$status_file")
    local battery_level_icon=''

    if [ -n "$battery" ]; then
        main "$power_supply" $charger_status "$exec_source"
    else
        battery_level_icon="$(get_battery_level_icon $charger_status 'Not found' 0)"
        notify_status "normal" "$battery_level_icon" "No existe batería" "$0 requiere batería para funcionar!"
    fi
}

# Validar terminal virtual, cuando power_supply.sh es llamdo desde un entorno distinto a una regla udev.
validate_virtual_terminal() {
    local -r power_supply="$1" charger_status=$2 xdg_session_type="$3" pid_xorg=$4 xdg_vtnr=$5
    local xdg_vtnr_xorg=''

    if [ "$xdg_session_type" == 'x11' ] || (( $pid_xorg != -1 )); then
        xdg_vtnr_xorg=$(xargs -0 -a /proc/$pid_xorg/environ printf "%s\n" | sed -En "s/^XDG_VTNR=([0-6])$/\1/p")
        if (( xdg_vtnr == xdg_vtnr_xorg )); then
	    validate_battery "$power_supply" $charger_status 'manual'
        else
	    echo "Xorg está en tty$xdg_vtnr_xorg, desde tty$xdg_vtnr no es posible ejecutar $0."
	fi
    elif [ "$xdg_session_type" == 'wayland' ]; then
	    validate_battery "$power_supply" $charger_status 'manual'
    fi
}

# Validar sesión. 
validate_session() {
	local -r power_supply="$1" charger_status=$2 devpath=$3 is_systemd_service="$4" xdg_vtnr=$5  xdg_session_type="$6" pid_xorg="$7" status_file='capacity'

    if [ "$xdg_session_type" == 'x11' ] || (( "$pid_xorg" != -1 )); then
	# Llamado desde una regla udev?
        if [ "$devpath" != 'null' ]; then
	    validate_battery "$power_supply" $charger_status 'udev'
        elif [ "$is_systemd_service" == 'true' ]; then
	    validate_battery "$power_supply" $charger_status 'systemd'
	else
            validate_virtual_terminal "$power_supply" $charger_status $xdg_session_type $pid_xorg $xdg_vtnr   # Validar ejecución manual.
	fi
    elif [ "$xdg_session_type" == 'tty' ]; then
        echo "Error, power_supply solo se puede ejecutar en una sesión Xorg."
#        echo "Error, power_supply can only run in one Xorg session."
    elif [ "$xdg_session_type" == 'wayland' ]; then
        if [ "$devpath" != 'null' ]; then
	    validate_battery "$power_supply" $charger_status 'udev'
        elif [ "$is_systemd_service" == 'true' ]; then
	    validate_battery "$power_supply" $charger_status 'systemd'
	else
            validate_virtual_terminal "$power_supply" $charger_status $xdg_session_type $pid_xorg $xdg_vtnr   # Validar ejecución manual.
	fi
    elif [ "$xdg_session_type" != '' ]; then
	echo 'Error, sesión no establecida.'
    else
	echo "Error, sesión $xdg_session_type desconocida."
    fi
}

# Obtener tipo de sesión.
get_session_type(){
    local -r id_session=$(loginctl | sed -En "s/.+([0-9]) .+$(whoami) seat.+/\1/p")
    local -r session_type=$(loginctl show-session $id_session -p Type --value)
    echo "$session_type"
}

## Variables globales.
#readonly POWER_SUPPLY='/home/dakataca/Git/GitLab/scripts/power-supply/power_supply' DEVPATH=${DEVPATH:-'null'} CRITICAL_BATTERY_LEVEL=15 # Modo de pruebas.
readonly POWER_SUPPLY='/sys/class/power_supply' DEV_PATH=${DEVPATH:-"null"} CRITICAL_BATTERY_LEVEL=20 DEFAULT_XDG_VTNR=${XDG_VTNR:-"1"}
readonly CHARGER_STATUS=${1:-$(get_current_charger_status "$POWER_SUPPLY")} IS_SYSTEMD_SERVICE="${IS_SYSTEMD_SERVICE:-"false"}" \
	PID_XORG=$(pgrep -x Xorg 2>/dev/null || echo '-1') XDG_SESSION_TYPE=${XDG_SESSION_TYPE:-$(get_session_type)} 

#validate_session "$POWER_SUPPLY" $CHARGER_STATUS "$DEV_PATH" "$IS_SYSTEMD_SERVICE" $DEFAULT_XDG_VTNR "$XDG_SESSION_TYPE"
validate_session "$POWER_SUPPLY" $CHARGER_STATUS "$DEV_PATH" "$IS_SYSTEMD_SERVICE" $DEFAULT_XDG_VTNR "$XDG_SESSION_TYPE" $PID_XORG
