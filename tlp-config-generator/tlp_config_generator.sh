#!/usr/bin/bash
# Automatizar las configuraciones de ahorro de energía del procesador con TLP.

## Emojis y tablas:
# └ ┘ ┌  ┐ ┬ ├ ┤ ㄱ ㆍ │
#                       ✅
# ╚ ╝ ╔ ╗ ═ ║ ╠ ╣ ╬ ╢ ╟ ╩ ╦ ╙ ╨ ╓ ─ 

# Obtener información de ahorro de energía del procesador.
get_processor_info(){
    local -r processor_info=$(sudo tlp-stat --processor)
    echo "$processor_info"
}

# Obtener información de TLP.
get_tlp_info(){
    local -r tlp_info=$(sudo tlp-stat)
    echo "$tlp_info"
}

# Consultar valores del ahorro de energía del procesador a partir de un patrón.
get_processor_values(){
    local -r tlp_info="$1" pattern="$2"
    local value=${tlp_info#*${pattern}*= }; value=${value%%/*}
    echo "$value"
}

#  ╔╦════════════════════════════════════════════════════════════════════════════════════════════════════════════╦╗
#  ║╚╣  Selecciona un elemento del menú de opciones generado a partir del parámetro array "("$@")"             ╠╝║
#  ╠╣  PARAMETERS╚╣  LOCAL╚════════════╣  OUTPUTS╚═╗  ────────────────────────────────────────────────────────╢
#  ║╠╬ㆍ"$1"       ╬ㆍ"$prompt"          ╬════════════╬ Prompt para el bloque select.                             ║
#  ║╠╬ㆍ"("$@")"   ╬ㆍ"${menu_items[@]}" ╬════════════╬ Array de elementos para el bloque select.                 ║
#  ║╠╬═════════════╬ㆍ"$num_items        ╬════════════╬ Número de elementos del array ${#menu_items[@]}.          ║
#  ║╚╬═════════════╬ㆍ"$selected_items"  ╬     ✅     ╬ Elemento seleccionado del menú de opciones.               ║
#  ╠╣  GLOBAL╚════╩═════╗  ────────────╨────────────╨───────────────────────────────────────────────────────────╢
#  ║╚╬ㆍ"$PS3"           ╬ Mensaje del prompt del bloque select.                                                  ║
#  ╠╣  RETURNS╚═════════╣  ─────────────────────────────────────────────────────────────────────────────────────╢
#  ║╚╬ㆍ"$selected_item" ╬ Obliga a elegir única y exclusivamente el item seleccionado del menú de opciones.      ║
#  ╚═════════════════════╩════════════════════════════════════════════════════════════════════════════════════════╝
select_item_options_menu(){
    local -r prompt="$1"; shift            # Obtener y eliminar el primer parámetro.
    local -ar menu_items="("$@")"
    local num_items="1" selected_item=''

    for (( i=1; i<${#menu_items[@]}; i++ )); do
        num_items+="|$((i+1))"
    done

    PS3="$prompt: "
    select item in "${menu_items[@]}"; do
        eval "case \$REPLY in
            ($num_items) selected_item=\$item; break;;
        esac"
    done
    echo "$selected_item"
}

# Obtener configuración por defecto de TLP.
get_tlp_default_config(){
    local -r file=/usr/share/tlp/defaults.conf
    local -r default_config="$(cat "$file")"
    echo "$default_config"
}

# Obtener item por defecto.
get_default_cpu_scalling_controller(){
    local -ar menu_items="("$@")"


}

# Función principal.
main(){
    local -r tlp_info="$(get_tlp_info)"
    local -r scaling_driver="$(get_processor_values "$tlp_info" 'scaling_driver')" \
            scaling_throttles="$(get_processor_values "$tlp_info" 'scaling_available_governors')"  tlp_default_config=$(get_tlp_default_config)
    local -r scaling_throttle="$(select_item_options_menu 'Elija su regulador de escalado de cpu para el modo AC' "${scaling_throttles[@]}")"
    echo "$scaling_driver"
    echo "$scaling_throttle"

}

main
