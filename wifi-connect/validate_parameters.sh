#!/bin/bash
# Escanea las redes disponibles usando los eventos generados por wpa_cli.

## Emojis y tablas:
#             
# └ ┘ ┌  ┐ ┬ ├ ┤ ㄱ ㆍ │
#                       ✅
# ╚ ╝ ╔ ╗ ═ ║ ╠ ╣ ╬ ╢ ╟ ╩ ╦ ╙ ╨ ╓ ─
# Ⅰ  Ⅱ  Ⅲ  Ⅳ  Ⅴ  Ⅵ  Ⅶ  Ⅷ  Ⅸ  Ⅹ  Ⅺ  Ⅻ  Ⅼ  Ⅽ  Ⅾ  Ⅿ
#  ⅰ  ⅱ  ⅲ  ⅳ  ⅴ  ⅵ  ⅶ  ⅷ  ⅸ  ⅹ  ⅺ  ⅻ  ⅼ  ⅽ  ⅾ  ⅿ 

# Función para importar una sola función desde otro script.
import_function() {
    local script_path="$1"
    local function_name="$2"
    
    if [[ ! -f "$script_path" ]]; then
        echo "Error: El script '$script_path' no existe." >&2
        return 1
    fi
    
    # Verificar si la función ya está definida.
    if declare -F "$function_name" > /dev/null; then
#        echo "La función '$function_name' ya está definida. No se volverá a importar."
        return 0
    fi

    # Usar una subshell para evitar contaminar el entorno actual.
    local fn_def
    fn_def=$(
        source "$script_path"
        declare -f "$function_name"
    )
    
    # Verificar si la función existe
    if [[ -z "$fn_def" ]]; then
        echo "Error: La función '$function_name' no se encontró en '$script_path'." >&2
        return 1
    fi

    # Evaluar la definición de la función en el contexto actual.
    eval "$fn_def"
}

# Finalizar script con mensaje de error.
end_script_with_message(){
    local msg="${1%.}"     # Eliminar punto final.
    msg=${msg/ de el / del }
    echo "$msg."
    exit 1
}

# Validar nombre de una función Bash y obtener mensaje final que hace referencia a ella.
validate_function_name(){
    local -r name_function="$1" alnum='[a-zA-Z0-9]'
    local -r regex_var=".+" regex_alert_cod='[1-9][0-9]+?'
    local -r regex_valid_var="^$regex_var:$regex_alert_cod$" regex_valid_function="^[a-zA-Z]+$alnum+(_$alnum+)+?$" \
	    regex_nameless_var="^$regex_var?:$regex_alert_cod$" regex_no_cod_var="^$regex_var:$regex_alert_cod?$" \
	    regex_invalid_var="^$regex_var?:$regex_alert_cod?$"
    local validated_function_name=''

    if [[ -z "$name_function" ]]; then
        end_script_with_message "Nombre de función no establecido"
    elif [[ "$name_function" =~ $regex_valid_function ]]; then
	validated_function_name="la función \"$name_function()\""
    elif [[ "$name_function" =~ ^$regex_var\.sh$ ]]; then
        validated_function_name="el script \"$name_function\""
    elif [[ "$name_function" =~ $regex_valid_var ]]; then
        end_script_with_message "Hace falta el nombre de la función, \"$name_function\" es un parámetro en formato validación"
    elif [[ "$name_function" =~ $regex_invalid_var ]]; then
        end_script_with_message "Hace falta el nombre de la función, \"$name_function\" es un parámetro en formato validación incompleto"
    else
        end_script_with_message "Nombre de función con formato incorrecto \"$name_function\", debería ser *_*"
    fi
    echo "$validated_function_name"
}

# Validar código de alerta.
validate_alert_cod(){
    local -r name_function="$1" param="$2" alert_cod=$3
    local -a alerts=(); readarray -t alerts < <(get_alerts_error_message)
    local -r num_alerts=${#alerts[@]}

    if (( alert_cod < 0 || alert_cod >= num_alerts )); then
            end_script_with_message "Advertencia, código de alerta \"$alert_cod\" del parámetro \"$param\" en $name_function(), \
debe estar dentro del rango 1-$(($num_alerts-1))."
    fi
}

# Obtener alerta según el código de alerta.
get_alert(){
    local -r name_function="$1" param="$2" alert_cod=$3
    validate_alert_cod "$name_function" "$param" $alert_cod
    local -a alerts=(); readarray -t alerts < <(get_alerts_error_message)
    echo "${alerts[$alert_cod]}"
}

# Validar que número elementos de array de parámetros es mayor o igual a dos (2).
validate_num_parameters(){
    local -r name_function="$1" num_parameters=$2
    if (( num_parameters < 2 )); then
	    end_script_with_message "Uno de los parámetros de $name_function está vacío, se necesita al menos $((2-$num_parameters)) más válido."
    fi
}

# Validar formato de parámetros pasados a una función.
validate_parameter_format(){
    local -ar parameters=("$@")
    local -r name_function="${parameters[0]}" num_parameters=${#parameters[@]}
    validate_num_parameters "$name_function" $num_parameters
    local -ar variables=(${parameters[@]:1:$(($num_parameters-1))})
    local -r regex_var=".+" regex_alert_cod='[0-9]+'
    local -r regex_valid_var="^$regex_var:$regex_alert_cod$"
    local alert_cod=0 alert='' msg=''


    for i in "${variables[@]}"; do
        alert_cod="${i#*:}"
        if [[ "$i" =~ $regex_valid_var ]]; then
	    validate_alert_cod "$name_function" "$i" $alert_cod
    	    continue
        else
            if [[ ! "$i" =~ ^.*:.*$ ]]; then
                msg="Error, se requiere el formato \"var:cod\" para poder validar \"$i\" como parámetro en $name_function"
            elif [[ "$i" =~ ^$regex_var:$ ]]; then
                msg="Por segudirad debe establecer un código de error al parámetro \"$i\" en $name_function"
            elif [[ "$i" =~ ^:$regex_alert_cod$ ]]; then
	        validate_alert_cod "$name_function" "$i" $alert_cod
                import_function "scan-networks.sh" "get_alerts_error_message"  # Importar función con el array de mensajes de error.
		alert=$(get_alert "$name_function" "$i" $alert_cod)
                msg="Error del parámetro \"$i\" en $name_function, le hace falta $alert"
            elif [[ "$i" =~ ^:$ ]]; then
                msg="Error del parámetro \"$i\" en $name_function, hacen falta la variable y el código de error"
            else
                msg="Formato de parámetro \"$i\" incorrecto en $name_function"
	    fi
            end_script_with_message "$msg"
        fi
    done
}

# Validar parámetros obligatorios de una función referenciándola.
validate_parameters(){
    local -ar parameters=("$@")
    local -a variables=()
    local num_parameters=0 name_function=''

    if [[ -z "${parameters[@]}" ]]; then
        end_script_with_message "Debe asignar al menos dos parámetros a validar en ${FUNCNAME[0]}()"
    fi
    if ! name_function="$(validate_function_name "${parameters[-1]}")"; then
        end_script_with_message "$name_function"
    fi
    num_parameters=${#parameters[@]}

    if (( num_parameters >= 2 )); then
        variables=(${parameters[@]:0:$(($num_parameters-1))})    # Obtener parámetros del array "${parameters[@]}" excepto el último.
        validate_parameter_format "$name_function" ${variables[@]}
        return 0
    else
        end_script_with_message "Se requiere al menos un parámetro en formato \"var:cod\" justo antes de $name_function"
    fi
}


# Evalúa el número de parámetros que requiere una función Bash para ser llamada correctamente.
get_function_num_parameters(){
    local -r script_name="$1" function_name="$2"
    local right_content='' script_content='' function_content="" function_name_obtained='' index=1 num_parameters=-1

    if ! script_content="$(cat $script_name)"; then
        echo "Finalizar script con error al intentar obtener contenido del script $script_name."
    fi

    script_content="$(cat $script_name)"
    right_content="${script_content//*$function_name()\{/$function_name()\{}"
    function_content="${right_content%%$'\n'\}*}"
    function_name_obtained="${function_content%%\{*}"
    function_name_obtained="${function_name_obtained%%(*}"
    function_name_obtained="${function_name_obtained// /}"

    if [ "$function_name" != "$function_name_obtained" ]; then
        echo "Finalizar script con función \"$function_name\" no existe en el script \"$script_name\"."
    fi

    while [[ "$function_content" =~ \$${index} ]]; do
            ((index++))
    done
    num_parameters=$((index-1))
    echo "La función $function_name del script $script_name requiere $num_parameters parámetros."
}

get_function_num_parameters "$1" "$2"
