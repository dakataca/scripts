#!/bin/bash

## Emojis y tablas:
#       
# └ ┘ ┌  ┐ ┬ ├ ┤ ㄱ ㆍ │
#                       ✅
# ╚ ╝ ╔ ╗ ═ ║ ╠ ╣ ╬ ╢ ╟ ╩ ╦ ╙ ╨ ╓ ─

# Obtener nombre de la interface inalámbrica.
get_wlan(){
    local -r wlans="$(ls /sys/class/net/)"
    local wlan="${wlans/*$'\n'wl/wl}"
    wlan="${wlan%%$'\n'*}"
    echo "$wlan"
}

# Obtener estado completo wpa_supplicant.
get_full_status() {
    local -r wlan="$1"
    local -r wpa_full_status="$(wpa_cli -i $wlan status)"
    echo "$wpa_full_status"
}

# Obtener valor de la opción.
get_value_option(){
    local -r option="$1" wpa_full_status="$2"
    local value="${wpa_full_status##*$option=}"             # Elimina todo antes de '$option=' y conserva lo que sigue.
    value="${value%%$'\n'*}"             # Elimina todo lo que sigue después del primer salto de línea.
    echo "$value"
}

# Obtener red WI-FI a la que estoy conectado.
get_network_connected(){
    local -r wpa_status="$1" wpa_full_status="$2"
    local network_connected=''

    if [ "$wpa_status" == 'COMPLETED' ] && [ -n "$wpa_full_status" ]; then
        network_connected="$(get_value_option "ssid" "$wpa_full_status")"
    else
	network_connected="wpa_state=$wpa_state, it is not connected to any Wi-Fi network."
    fi
    echo "$network_connected"
}

# Escanear redes WI-FI disponibles.
scan_wifi(){
    local -r interface="$1"
    wpa_cli -i "$interface" scan
}

# Validar según el estado si es necesario escanear o no.
validate_by_state_scan(){
    local -r interface="$1" wpa_current_state="$2"
    local necessary_scan='not'

#    if [ "$wpa_current_status" != 'SCANNING' ]; then
#        
#
#    fi
    echo "$necessary_scan"
}

# Funsión principal -----------------------------------------------------------------------------------------------------------------------------------
main(){
    local -r wlan="$(get_wlan)"
    local -r wpa_full_status="$(get_full_status "$wlan")"
    local -r wpa_state="$(get_value_option 'wpa_state' "$wpa_full_status")"
    local -r network_connected="$(get_network_connected "$wpa_state" "$wpa_full_status")"
    echo "$network_connected"
}
main
