#!/bin/bash
# Escanea las redes disponibles usando los eventos generados por wpa_cli.

## Emojis y tablas:
#             
# └ ┘ ┌  ┐ ┬ ├ ┤ ㄱ ㆍ │
#                       ✅
# ╚ ╝ ╔ ╗ ═ ║ ╠ ╣ ╬ ╢ ╟ ╩ ╦ ╙ ╨ ╓ ─
# Ⅰ  Ⅱ  Ⅲ  Ⅳ  Ⅴ  Ⅵ  Ⅶ  Ⅷ  Ⅸ  Ⅹ  Ⅺ  Ⅻ  Ⅼ  Ⅽ  Ⅾ  Ⅿ
#  ⅰ  ⅱ  ⅲ  ⅳ  ⅴ  ⅵ  ⅶ  ⅷ  ⅸ  ⅹ  ⅺ  ⅻ  ⅼ  ⅽ  ⅾ  ⅿ 

# Obtener alerta para el mensaje de error cuando se validan variables obligatorias en validate_parameters.sh.
get_alerts_error_message(){
    local -r name_function="$1" var="$2" param="$3"
    local -ar alerts=(
"Código de error del parámetro \"$param\" debe ser mayor a cero "0" en $name_function()"
"el nombre de la interface inalámbrica"
"la ruta del fichero log"
"el último evento generado de wpa_cli"
"el patrón que se repite en los eventos generados por wpa_cli"
"el nombre de la herramienta CLI"
"el nombre de la variable"
"Código de error de la variable \"$var\" de la función \"$name_function()\" aún no establecido en \"${FUNCNAME[0]}()\""
)
    printf "%s\n" "${alerts[@]}"
}

# Terminar ejecución de herramienta cli.
finish_running_cli_tool(){
    local -r name_tool="$1"
    validate_parameters "$name_tool:5" "${FUNCNAME[0]}"
    if pgrep -f "$name_tool" > /dev/null; then
	echo "Finalizando herramienta CLI $name_tool..."
        pkill -f $name_tool> /dev/null
	echo "Herramienta CLI $name_tool finalizada."
    fi
}

# Terminar wpa_cli si se encuentra en ejecución y eliminar fichero log si existe.
cleanup(){
    local -r log_file="$1"
    validate_parameters "$log_file:2" "${FUNCNAME[0]}"
    finish_running_cli_tool 'wpa_cli'
    [ -f "$log_file" ] && rm -rf "$log_file"
}

#  ╔╦═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════════╦╗
#  ║╚═╬═══╣                     Validar evento generado del modo interactivo de wpa_cli                             ╠═══╬═╝║
#  ╠╣  ╚════════════════════════╣  ╚══════════════╣  ╚════════╗  ────────────────────────────────────────────────────────╢
#  ║╠╬ㆍ"$1"                     ╬ㆍ"$event"        ╬      Ⅰ     ╬ Último evento generado en el modo interactivo de wpa_cli. ║
#  ║╠╬ㆍ"$2"                     ╬ㆍ"$ctrl_event"   ╬      Ⅰ     ╬ Patrón que se repite en los eventos generados por wpa_cli.║
#  ║╚╬═══════════════════════════╬ㆍ"$event_action" ╬          ╬ Acción del evento generado por wpa_cli.                   ║
#  ╠╣  ╚════════════════════════╣  ───────────────╨────────────╨───────────────────────────────────────────────────────────╢
#  ║╚╬Ⅰ validate_parameters()   ╬ Validar parámetros obligatorios.                                                          ║
#  ╚═╩═══════════════════════════╩═══════════════════════════════════════════════════════════════════════════════════════════╝
# Validar evento generado del modo interactivo de wpa_cli.
validate_event(){
    local -r event="$1" ctrl_event="$2"
    validate_parameters "$event:3" "$ctrl_event:4" "${FUNCNAME[0]}"
    local event_action=''

    case "$event" in
        "${ctrl_event}NETWORK-NOT-FOUND") event_action='No está conectado a ninguna red WI-FI.';;
        "${ctrl_event}SCAN-RESULTS") event_action='Conectado a una red wi-FI.';;
        "${ctrl_event}*") event_action="Evento \"$event\" aún no validado.";;
        "*") event_action="Evento desconocido: $event.";;
    esac
    echo "$event_action"
}

#  ╔╦═══════════════════════════════════════════════════════════════════════════════════════════════════════════════════╦╗
#  ║╚═╬═══╣                   Obtener último evento generado del modo interactivo de wpa_cli                    ╠═══╬═╝║
#  ╠╣  ╚════════════════════════╣  ╚══════════════╣  ╚═══╗  ─────────────────────────────────────────────────────────╢
#  ║╠╬ㆍ"$1"                     ╬ㆍ"$log_file"     ╬  Ⅰ,Ⅱ  ╬ Fichero log del script wifi-connet.sh.                     ║
#  ║╠╬═══════════════════════════╬ㆍ"$event"        ╬   Ⅲ   ╬ Último evento generado en el modo interactivo de wpa_cli.  ║
#  ║╠╬═══════════════════════════╬ㆍ"$ctrl_event"   ╬   Ⅲ   ╬ Patrón que se repite en los eventos generados por wpa_cli. ║
#  ║╚╬═══════════════════════════╬ㆍ"$time"         ╬   Ⅳ   ╬ Segundos de espera mientras se obtiene evento de wpa_cli.  ║
#  ╠╣  ╚════════════════════════╣  ───────────────╨───────╨────────────────────────────────────────────────────────────╢
#  ║╠╬Ⅰ validate_parameters()   ╬ Validar parámetros obligatorios.                                                      ║
#  ║╠╬Ⅱ cleanup()               ╬ Terminar ejecución de wpa_cli eliminar si existe el fichero log.                      ║
#  ║╚╬Ⅲ validate_event()        ╬ Validar último evento generado del modo interactivo de wpa_cli.                       ║
#  ╠╣ ╚═════════════════════════╣  ────────────────────────────────────────────────────────────────────────────────────╢
#  ║╚╬Ⅳ sleep                   ╬ Esperar segundos mientras se obtiene evento de wpa_cli.                               ║
#  ╚═╩═══════════════════════════╩═══════════════════════════════════════════════════════════════════════════════════════╝
get_last_event(){
    local -r log_file="$1" ctrl_event='CTRL-EVENT-'
    validate_parameters "$log_file:2" "${FUNCNAME[0]}"
    local event='' time=2

    > "$log_file"
    while pgrep -f wpa_cli > /dev/null; do 
	if [[ ! "$event" =~ "$ctrl_event" ]]; then
            sleep $time 
	    event="$(tr -d '\0' < "$log_file")"
	else
            cleanup "$log_file"
            event="${event##*<3>}"
            event="${event%%[[:space:]]$'\n'*}"           
            validate_event "$event" "$ctrl_event"
	fi
	time=$(echo "scale=2; $time / 2 + 0.33" | bc)
    done
}

# Verificar si el wifi está bloqueado vía hardware y/o software.
verify_wifi_bloqued(){
    local -r rfkill_list="$(rfkill list)"
    local wifi_rfkill=${rfkill_list##*Wireless LAN} soft_bloqued='' hard_bloqued='' lock_level=''
    wifi_rfkill=${wifi_rfkill%%[0-9]:*}
    soft_blocked=${wifi_rfkill##*Soft blocked: }
    hard_blocked=${soft_blocked##* }
    soft_blocked=${soft_blocked%%$'\n'*}
    hard_blocked=${hard_blocked%%$'\n'*}
#    echo "\"$soft_blocked\" y "\"$hard_blocked\""

    if [ "$soft_blocked" == 'no' ] && [ "$hard_blocked" == 'no' ]; then
       return 0
    elif [ "$soft_blocked" == 'yes' ] && [ "$hard_blocked" == 'no' ]; then
        lock_level="software, ejecute 'rfkill unblock wifi' para desbloquearlo"
    elif [ "$soft_blocked" == 'no' ] && [ "$hard_blocked" == 'yes' ]; then
        lock_level='hardware.'
    elif [ "$soft_blocked" == 'yes' ] && [ "$hard_blocked" == 'yes' ]; then
        lock_level='hardware y software'
    fi
    echo "WI-FI bloqueado a nivel de $lock_level."
}

#  ╔╦════════════════════════════════════════════════════════════════════════════════════════════════════════════╦╗
#  ║╚═╬═══╣                          Escanear redes WI-FI disponibles usando wpa_cli                     ╠═══╬═╝║
#  ╠╣  ╚════════════════════════╣  ╚════════════╣  ╚════════╗  ───────────────────────────────────────────────╢
#  ║╠╬ㆍ"$1"                     ╬ㆍ"$interface"  ╬      Ⅰ     ╬ Interface de red inalámbrica.                    ║
#  ║╠╬ㆍ"$2"                     ╬ㆍ"$log_file"   ╬     Ⅰ,Ⅱ    ╬ Fichero log del script wifi-connet.sh.           ║
#  ║╠╬═══════════════════════════╬ㆍ"$scanning"   ╬════════════╬ Resultado de si está escaneando correctamente.   ║
#  ╠╣  ╚════════════════════════╣  ─────────────╨────────────╨──────────────────────────────────────────────────╢
#  ║╠╬Ⅰ validate_parameters()   ╬ Validar parámetros obligatorios.                                               ║
#  ║╚╬Ⅱ get_last_event()        ╬ Obtener y validar último evento generado del modo interactivo de wpa_cli.      ║
#  ╠╣ ══╝ wpa_cli -i wlan0 ╚════╣  ─────────────────────────────────────────────────────────────────────────────╢
#  ║╠╬ㆍscan                     ╬ Iniciar escaneo de redes con wpa_cli.                                          ║
#  ║╠╬ㆍ> $log_file              ╬ Escribir eventos generados por wpa_cli en el fichero $log_file.                ║
#  ║╚╬ㆍscan_results             ╬ Mostrar resultados del escaneo de redes de wpa_cli.                            ║
#  ╚═╩═══════════════════════════╩════════════════════════════════════════════════════════════════════════════════╝
scan_networks(){
    local -r interface="$1" log_file="$2"
    validate_parameters "$interface:1" "$log_file:2" "${FUNCNAME[0]}"
    local -r scanning="$(wpa_cli -i "$interface" scan)"
    local get_last_event_pid=null

    if [ "$scanning" == 'OK' ]; then
        get_last_event "$log_file" &
#        get_last_event "" &
        get_last_event_pid=$!
	echo 'Scanning...'
        wpa_cli -i "$interface" > "$log_file"
        wait $get_last_event_pid
        if [ $? -eq 0 ]; then
            wpa_cli -i "$interface" scan_results
        else
	    echo 'Fallo al escanear redes.'
        fi
    elif [ "$scanning" == 'FAIL-BUSY' ] || [ "$scanning" == 'FAIL' ]; then
	echo "$scanning to scan."
        verify_wifi_bloqued
    else
	echo 'Failure to init scan.'
        verify_wifi_bloqued
    fi
}

# Funsión principal.
main(){
    local -r interface="$1" log_file="/tmp/wifi-connect.log"
    validate_parameters "$interface:1" "$log_file:7" "${FUNCNAME[0]}"
#    validate_parameters "$interface:1" ":6" "${FUNCNAME[0]}"
#    validate_parameters "$interface:1"
#    validate_parameters "${FUNCNAME[0]}"
    cleanup "$log_file"
    scan_networks "$interface" "$log_file"
}

source validate_parameters.sh
#validate_parameters "$0"
validate_parameters "$1:1" "$0"
#validate_parameters "v:" "$0"
#validate_parameters ":1" "$0"
#validate_parameters "$1" "$0"
#validate_parameters "$0"
main "$1"
