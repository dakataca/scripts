#!/bin/bash

## Emojis y tablas:
#       
# └ ┘ ┌  ┐ ┬ ├ ┤ ㄱ ㆍ │
#                       ✅
# ╚ ╝ ╔ ╗ ═ ║ ╠ ╣ ╬ ╢ ╟ ╩ ╦ ╙ ╨ ╓ ─

# Obtener nombre de la interface inalámbrica.
get_wlan(){
    local -r wlans="$(ls /sys/class/net/)"
    local wlan="${wlans/*$'\n'wl/wl}"            # Remplace todo lo que está a la izquierda de wl por nada.

    if [[ "$wlan" == wl*$'\n'* ]]; then            # ¿$wlans contiene salto de línea?
        wlan="${wlan%%$'\n'*}"
    fi
    echo "$wlan"
}

# Obtener estado de wpa_supplicant.
get_wpa_supplicant_status() {
    local -r interface="$1" 
    local -r wpa_full_status="$(wpa_cli -i "$interface" status)"
    local wpa_status="${wpa_full_status##*wpa_state=}"             # Elimina todo antes de 'wpa_state=' y conserva lo que sigue.
    wpa_status="${wpa_status%%$'\n'*}"             # Elimina todo lo que sigue después del primer salto de línea.
    echo "$wpa_status"
}

# Obtener nombre de la red a la que estoy conectado (COMPLETED).
get_network_connected(){
    local -r interface="$1"
    local -r wpa_full_status="$2"
    local ssid="${wpa_full_status##*ssid=}"                # Elimina todo antes de 'ssid=' y conserva lo que sigue.
    ssid="${ssid%%$'\n'*}"                     # Elimina todo lo que sigue después del primer salto de línea.
    echo "$ssid"
}

# Escanear redes.
scan_wifi(){
    local -r interface="$1" available_wifi_networks="$2" waiting_time=.3
    local -r wpa_status="$(get_wpa_supplicant_status "$interface")"
    local wifi_list='' new_wpa_status='' count=0 network_connected=''

    if [[ "$wpa_status" =~ INACTIVE|DISCONNECTED ]]; then
        wpa_cli -i "$interface" scan > /dev/null &
        if [ "$wpa_status" == 'DISCONNECTED' ]; then
            new_wpa_status="$wpa_status"
            while [[ ! "$new_wpa_status" =~ INACTIVE|COMPLETED ]]; do
                new_wpa_status="$(get_wpa_supplicant_status "$interface")"
		sleep $waiting_time 
	    done
        elif [ "$wpa_status" == 'INACTIVE' ]; then
            wifi_list=$(wpa_cli -i "$interface" scan_results)
            while [[ ! "$wifi_list" =~ [0-9a-f:]{17} ]] && (( count <= 10 )); do
		((count++))
		sleep .6 
                wifi_list=$(wpa_cli -i "$interface" scan_results)
	    done 
            [[ ! "$wifi_list" =~ [0-9a-f:]{17} ]] && sleep 2
        fi
    elif [ "$wpa_status" == 'SCANNING' ]; then
        wifi_list=$(wpa_cli -i "$interface" scan_results)
        if [[ ! "$wifi_list" =~ [0-9a-f:]{17} ]]; then
            sleep $waiting_time 
            wifi_list=$(wpa_cli -i "$interface" scan_results)
        fi
    elif [ "$wpa_status" == 'COMPLETED' ]; then
        wpa_full_status="$(wpa_cli -i "$interface" status)"
        network_connected="$(get_network_connected "$interface" "$wpa_full_status")"
        wifi_list=$(wpa_cli -i "$interface" scan_results)
        wifi_list="${wifi_list/$network_connected/$network_connected\*}"
	wifi_list=$(clear_header "$wifi_list")
        if (( $(echo "$wifi_list" | wc -l) == 1 )); then
	    wifi_list=''
	    count=0
            wpa_cli -i "$interface" scan > /dev/null &
	    while (( $(echo "$wifi_list" | wc -l) == 1 )) && (( count <= 8)); do
                wifi_list=$(wpa_cli -i "$interface" scan_results)
	        wifi_list=$(clear_header "$wifi_list")
	        sleep 1
	    done
	fi
    fi
    wifi_list=${wifi_list:-$(wpa_cli -i "$interface" scan_results)}
    echo "$wifi_list"
}

# Eliminar cabecera del resultado del escaneo de redes.
clear_header(){
    local -r scan_result="$1"
    local wifi_list=''
        if [[ "$scan_result" =~ FAIL-BUSY ]]; then
            wifi_list="${scan_result#*$'\n'*$'\n'}"  # Eliminar las dos primeras líneas (encabezado).
        elif [[ "$scan_result" =~ "bssid / frequency / signal level / flags / ssid" ]]; then
            wifi_list="${scan_result#*$'\n'}"        # Eliminar solo la primera línea (encabezado). 
        else
            wifi_list="$scan_result"        # Eliminar solo la primera línea (encabezado). 
	fi
    echo "$wifi_list"
}

# Obtener redes disponibles.
get_available_wifi_networks(){
    local -r interface="$1" scan_result="$2"
    local wifi_list=''
    local -a available_wifi=()

    if [[ "$scan_result" =~ [0-9a-f:]{17} ]]; then
        wifi_list="$(clear_header "$scan_result")"
        while read -r line; do
            line="${line//*]/}"
            line="${line/[[:space:]]/}"
            if [[ ! "$line" =~ ^[[:space:]]+$ ]] && [ "$line" != '' ] ; then
                available_wifi+=(\"$line\")
            fi
        done <<< "$wifi_list"
    fi
    echo "${available_wifi[@]}"
}

#  ╔╦════════════════════════════════════════════════════════════════════════════════════════════════════════════╦╗
#  ║╚═╬═╬═╣  Selecciona un elemento del menú de opciones generado a partir del parámetro array "("$@")"  ╠═╬═╬═╝║
#  ╠╣  PARAMETERS╚╣  LOCAL╚════════════╣  OUTPUTS╚═╗  ────────────────────────────────────────────────────────╢
#  ║╠╬ㆍ"$1"       ╬ㆍ"$prompt"          ╬════════════╬ Prompt para el bloque select.                             ║
#  ║╠╬ㆍ"$2"       ╬ㆍ"$empty_menu_msg"  ╬           ╬ Mensaje si la lista de elementos para el menú esta vacía. ║
#  ║╠╬ㆍ"("$@")"   ╬ㆍ"${menu_items[@]}" ╬════════════╬ Array de elementos para el bloque select.                 ║
#  ║╠╬═════════════╬ㆍ"$item"            ╬════════════╬ Elemento iterado en el bloque select.                     ║
#  ║╚╬═════════════╬ㆍ"$selected_item"   ╬           ╬ Elemento seleccionado del menú de opciones.               ║
#  ╠╣  GLOBAL╚════╩══════╗  ───────────╨────────────╨───────────────────────────────────────────────────────────╢
#  ║╚╬ㆍ"$PS3"            ╬ Mensaje del prompt para el bloque select.                                             ║
#  ╠╣  RETURNS╚══════════╣  ────────────────────────────────────────────────────────────────────────────────────╢
#  ║╚╬ㆍ"$selected_item"  ╬ Obliga a elegir única y exclusivamente el item seleccionado del menú de opciones.     ║
#  ╚══════════════════════╩═══════════════════════════════════════════════════════════════════════════════════════╝
#select_item_options_menu(){
#    local -r prompt="$1"
#    local -r empty_menu_msg="$2"
#    shift 2                                                      # Correr puntero de los primeros dos elementos (eliminarlos).
#    local -ar menu_items="("$@")"                                # Crear un array a partir de los elementos restantes."
#    local selected_item=''
#
#    if (( ${#menu_items[@]} >= 1 )); then
#        PS3="$prompt: "
#        select item in "${menu_items[@]}"; do
#            if [ -n "$item" ]; then                            # Asegúrate de que la selección sea válida.
#                selected_item="$item"
#                break
#            else
#                echo "Unknown option, try selecting a menu item again!"
#            fi
#        done
#    else
#        selected_item="$empty_menu_msg"
#    fi
#    echo "$selected_item"
#}

select_item_options_menu(){
    local -r prompt="$1"
    local -r empty_menu_msg="$2"
    local -r timeout=$3                                      # Tiempo límite en segundos.
    shift 3                                                    # Desplazar los primeros tres elementos (prompt, empty_menu_msg, timeout).
    local -ar menu_items=("$@")                                # Crear un array con los elementos restantes.
    local selected_item=''
    local user_input=''

    if (( ${#menu_items[@]} >= 1 )); then
        PS3="$prompt: "
        # Bucle para esperar la entrada del usuario con timeout.
        while true; do
            read -t $timeout -p "Elige una opción en los próximos $timeout segundos o se tomará una acción predeterminada: " user_input
            if [ -n "$user_input" ]; then
                if [[ "$user_input" =~ ^[0-9]+$ ]] && (( user_input > 0 && user_input <= ${#menu_items[@]} )); then
                    selected_item="${menu_items[$((user_input-1))]}"
                    break
                else
                    echo "Opción desconocida, intenta seleccionar un elemento del menú nuevamente."
                fi
            else
                echo "Tiempo agotado. Tomando la acción predeterminada."
                selected_item="Acción predeterminada"          # Acción por defecto si no hay entrada.
                break
            fi
        done
    else
        selected_item="$empty_menu_msg"
    fi
    echo "$selected_item"
}

# Función principal.
main2(){
    local -r interface="$(get_wlan)"
    local prev_networks=()  # Array para almacenar las redes anteriores.
    local interval=10  # Intervalo de actualización en segundos.

    while true; do
        local wpa_status="$(get_wpa_supplicant_status "$interface")"
        local scan_result=$(scan_wifi "$interface" "$wpa_status")
        local -a available_wifi_networks=($(get_available_wifi_networks "$interface" "$scan_result" "$wpa_status"))
        
        # Solo actualizar la parte de las redes sin limpiar toda la pantalla.
        tput cup 0 0  # Mover el cursor a la posición (0,0).

        # Mostrar el menú de redes.
        if (( ${#available_wifi_networks[@]} >= 1 )); then
            echo "Redes Wi-Fi disponibles:"
            for i in "${!available_wifi_networks[@]}"; do
                if [[ ! " ${prev_networks[*]} " =~ " ${available_wifi_networks[$i]} " ]]; then
                    echo -e "\e[1;32m$i) ${available_wifi_networks[$i]} (Nuevo)\e[0m"  # Resaltar redes nuevas.
                else
                    echo "$i) ${available_wifi_networks[$i]}"
                fi
            done
        else
            echo "No hay redes Wi-Fi disponibles."
        fi
        prev_networks=("${available_wifi_networks[@]}")  # Actualizar las redes anteriores.
        sleep "$interval"  # Esperar antes del siguiente escaneo.
    done
}

# Move pointer to new position
move_pointer_new_position(){
    local row=0 col=0
    echo -en "\033[6n" > /dev/null               # Enviar la secuencia de escape para solicitar la posición del cursor.
    IFS=';' read -sdR -p $'\E[6n' row col        # Leer la respuesta de la terminal.
    row="${row#*[}"                              # Eliminar el primer carácter '['.
    col="${col//R/}"                             # Ajustar los valores.
    row=$((row - 2))
    col=$((col - 1))
    #echo "Fila: $row, Columna: $col"            # Mostrar la posición.
    #sleep 2
    tput cup $row $col                           # Mover el cursor a la nueva posición.
    tput el                                      # Borrar desde la posición del cursor hasta el final de la línea.
}

# Función principal.
main1(){
    echo "Iniciando función main"
    local -r interface="$(get_wlan)"
    local wpa_status='' wpa_status='' scan_result=''

    local -r wpa_status="$(get_wpa_supplicant_status "$interface")"
    local -r scan_result=$(scan_wifi "$interface" "$wpa_status")
    local -ar available_wifi_networks=($(get_available_wifi_networks "$interface" "$scan_result" "$wpa_status"))
    # Ejecutar la función en segundo plano y capturar el PID.
    select_item_options_menu 'Select the Wi-Fi network' 'No Wi-Fi networks available' "${available_wifi_networks[@]}" & 
    pid=$!
    
    # Esperar 8 segundos y luego matar el proceso si aún está en ejecución
    sleep 1
    if kill -0 "$pid" 2>/dev/null; then
        echo "El menú de selección está tardando demasiado. Terminando proceso..."
        kill "$pid"
    fi
}
#main

# Función principal.
main(){
    local -r interface="$(get_wlan)"
    local -r wpa_status="$(get_wpa_supplicant_status "$interface")"
    local -r scan_result=$(scan_wifi "$interface" "$wpa_status")
    local -ar available_wifi_networks=($(get_available_wifi_networks "$interface" "$scan_result" "$wpa_status"))
    select_item_options_menu 'Select the Wi-Fi network' 'No Wi-Fi networks available' 5 "${available_wifi_networks[@]}"
}
main
